= Configuració del servidor i variables de sistema
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

== Fitxer de configuració principal

La configuració de MariaDB es troba al directori `/etc/mysql`

*Qüestió 1*: Fes una còpia de seguretat del directori de configuració
del MariaDB. L'utilitzarem per consultar la configuració inicial en cas que
vulguem revertir algun dels canvis que farem en els següents apartats.

*Qüestió 2*: A la configuració, modifica el port de connexió, tant dels clients
com del servidor, al 3011.

*Qüestió 3*: Configura el servidor per tal que accepti com a màxim 10 connexions
simultànies.

*Qüestió 4*: La màquina virtual d'aquesta pràctica ve amb un usuari `testuser`
creat, amb contrasenya `super3`. Força que aquest usuari no pugui establir més
de 3 connexions simultànies, i no pugui realitzar més de 10 consultes per hora.

*Qüestió 5*: Guarda els canvis i reinicia el servidor.

*Qüestió 6*: Fes les comprovacions necessàries per confirmar que les
modificacions fetes funcionen correctament.

-  Comprova l'estat de les opcions que s'utilitzen per aquesta sessió client.
-  Prova d'establir més de 10 connexions simultànies.
-  Prova d'establir més de 3 connexions amb l'usuari `testuser`.

== Fitxers de configuració addicionals

*Qüestió 7*: Crea un fitxer de configuració addicional amb les següents opcions,
que s'han d'aplicar al client `mysql`:

- Contrasenya automàtica: `super3`
- Nom d'usuari: `user` (cal crear un nou usuari)
- Activa la compressió
- Mostra els avisos
- Afegeix un _prompt_ personalitzat que inclogui l'hora del sistema i la base de
dades.

Anomena aquest fitxer `my_opts.txt` i guarda'l al directori del teu usuari.

[NOTE]
====
No s'ha de fer que aquest fitxer es llegeixi automàticament.
====

*Qüestió 8*: Executa el client `mysql` indicant-li que utilitzi la configuració
addicional que acabem de crear, a més de la configuració habitual.

*Qüestió 9*: Confirma la configuració: comprova l'estat de les opcions que
s'utilitzen per aquesta sessió client.

== Variables del sistema

*Qüestió 10*: Comprova la codificació de caràcters que utilitza per defecte el
servidor. Modifica-la per tal que s'utilitza la codificació `utf8mb4`.

*Qüestió 11*: Crea una taula nova i comprova que realment s'està utilitzant
aquesta codificació de caràcters.

*Qüestió 12*: Obre una sessió contra el servidor i comprova el valor actual
del `SQL_MODE`. Quin és?

*Qüestió 13*: Afegeix les opcions ANSI i ONLY_FULL_GROUP_BY al valor
global de la variable `SQL_MODE`. Comprova si aquests canvis afecten o no a la
sessió que tens oberta actualment.

*Qüestió 14*: Obre una nova sessió i comprova si es veu afectada pels canvis al
`SQL_MODE` de l'apartat anterior.

*Qüestió 15*: Afegeix (o esborra si ja hi és) l'opció `STRICT_TRANS_TABLES`
només per a aquesta sessió. Comprova que el canvi afecta a aquesta sessió però
no a l'altra que tens oberta.

*Qüestió 16*: Fes que el valor actual del `SQL_MODE` sigui el valor per defecte
del servidor cada cop que arrenqui.
