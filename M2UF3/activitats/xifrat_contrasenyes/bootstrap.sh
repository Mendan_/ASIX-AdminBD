#!/bin/sh

apt-get update
apt-get install -y debconf-utils software-properties-common dirmngr
apt-key adv --recv-keys --no-tty --keyserver keyserver.ubuntu.com 0xF1656F24C74CD1D8
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirror.i3d.net/pub/mariadb/repo/10.4/debian buster main'
apt-get update
apt-get install -y mariadb-server
wget -nv http://downloads.mysql.com/docs/sakila-db.tar.gz
tar xzf sakila-db.tar.gz
mysql < sakila-db/sakila-schema.sql
mysql < sakila-db/sakila-data.sql
rm sakila-db.tar.gz
