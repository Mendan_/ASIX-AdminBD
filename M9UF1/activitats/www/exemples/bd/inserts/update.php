<?php
require_once 'Connection.php';

session_start();

try {
  if (!isset($_POST['name']) || !isset($_POST['id'])) {
    throw new Exception("Falten paràmetres.");
  }
  $name = trim($_POST['name']);
  $id = trim($_POST['id']);
  $conn = connect();
  $statement = $conn->prepare("UPDATE Facilities SET Name=:name WHERE Id=:id");
  $statement->bindParam(':name', $name);
  $statement->bindParam(':id', $id);
  $statement->execute();
  header('Location: index.php');
  exit();
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}

?>
