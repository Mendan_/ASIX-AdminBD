<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Formulari</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Més pas de paràmetres per GET</h1>
      <div class="row">
        <div class="col-md-12">
          <ul>
          <li><a href="exemple.php?var1=3&var2=5">Primer enllaç</a></li>
          <li><a href="exemple.php?var1=1&var2=3">Segon enllaç</a></li>
          <li><a href="exemple.php?var1=2&var2=8">Tercer enllaç</a></li>
          <li><a href="exemple.php?var1=qwerty&var2=0">Quart enllaç</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ul>
          <?php
          for ($n=0; $n<5; $n++) {
            echo "<li><a href='exemple2.php?var=$n'>Enllaç $n</a></li>";
          }
          ?>
          </ul>
        </div>
      </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
