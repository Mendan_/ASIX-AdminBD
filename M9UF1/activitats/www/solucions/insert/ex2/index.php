<?php
require_once 'Connection.php';

session_start();

function get_rooms() {
  $conn = connect();
  $statement = $conn->prepare("SELECT r.RoomNumber, rt.Id, rt.Name
    FROM Rooms r JOIN RoomTypes rt ON r.RoomTypeId=rt.Id ORDER BY r.RoomNumber");
  $statement->execute();
  $rooms = $statement->fetchAll();
  return $rooms;
}

function show_rooms($rooms) {
  if (sizeof($rooms)==0) {
    echo "<p>No hi ha cap habitació.</p>\n";
  }
  ?>
  <table class='table table-striped'>
  <tr><th>Número d'habitació</th><th>Tipus d'habitació</th><th>Acció</th></tr>
  <?php
  foreach ($rooms as $room) {
    echo "<tr><td>{$room['RoomNumber']}</td><td>{$room['Name']}</td><td><a href='ex2.php?roomNumber={$room['RoomNumber']}'>Esborra</a></td></tr>\n";
  }
  ?>
  </table>
  <?php
}

function show_messages() {
  if (isset($_SESSION['error'])) {
    echo "<div class='alert alert-danger' role='alert'>{$_SESSION['error']}</div>";
    unset($_SESSION['error']);
  }
  if (isset($_SESSION['success'])) {
    echo "<div class='alert alert-success' role='alert'>{$_SESSION['success']}</div>";
    unset($_SESSION['success']);
  }
}
?>
<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Sentències INSERT</title>
  </head>
  <body>
    <?php show_messages(); ?>
    <main role="main" class="container">
      <h1 class="mt-5">Exercici 2</h1>
      <?php
      try {
        show_rooms(get_rooms());
      } catch (Exception $e) {
        $error = $e->getMessage();
        echo "<div class='alert alert-danger' role='alert'>No s'ha pogut recuperar la llista d'habitacions: $error</div>";
      }
      ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
