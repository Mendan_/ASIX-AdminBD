<?php
session_start();
?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Sessions</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Exercici 1</h1>
      <form action="resultat.php" method="post">
        <div class="form-group">
          <label for="num1">Primer nombre</label>
          <input type="number" class="form-control" name="num1" id="num1" placeholder="Escriu el primer nombre">
        </div>
        <div class="form-group">
          <label for="num2">Segon nombre</label>
          <input type="number" class="form-control" name="num2" id="num2" placeholder="Escriu el segon nombre">
        </div>
        <div class="form-group">
          <label for="operacio">Operació</label>
          <input type="text" class="form-control" name="operacio" id="operacio" placeholder="Selecciona l'operació">
        </div>
        <button type="submit" class="btn btn-primary">Envia</button>
      </form>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
