<?php
require_once 'Connection.php';

function getRoomNumber() {
    if (!isset($_POST['roomnumber'])) {
      throw new Exception("Falten paràmetres.");
    }
    $roomNumber = trim($_POST['roomnumber']);
    if (!is_numeric($roomNumber)) {
      throw new Exception("El número d'habitació ha de ser un nombre.");
    }
    return $roomNumber;
}

function getRoomType($conn, $roomNumber) {
  $statement = $conn->prepare("SELECT Name FROM RoomTypes rt JOIN Rooms r ON
    r.RoomTypeId=rt.Id WHERE r.RoomNumber=:roomNumber");
  $statement->bindParam(':roomNumber', $roomNumber);
  $statement->execute();
  $results = $statement->fetchAll();
  if (sizeof($results)==0) {
    throw new Exception("No existeix el número d'habitació.");
  }
  return $results[0]['Name'];
}

session_start();
try {
  $roomNumber = getRoomNumber();
  $conn = connect();
  $roomTypeName = getRoomType($conn, $roomNumber);
} catch(PDOException $e) {
  $_SESSION['error'] = "No s'ha pogut recuperar el tipus d'habitació:\n{$e->getMessage()}\n";
  header('Location: index.php');
  exit();
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}
?>
<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Consulta d'habitació</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Cerca una habitació</h1>
      <?php echo("L'habitació $roomNumber és de tipus $roomTypeName"); ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
