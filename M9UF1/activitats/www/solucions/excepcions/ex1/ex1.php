<?php
session_start();
try {
  if (!isset($_POST['num1']) || !isset($_POST['num2'])) {
    throw new Exception("Falten paràmetres.");
  }
  $num1 = $_POST['num1'];
  $num2 = $_POST['num2'];
  if (!is_numeric($num1) || !is_numeric($num2)) {
    throw new Exception("S'esperaven nombres.");
  }
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}

?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Excepcions</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Exercici 1</h1>
      <div>
        <code><pre><?php
        if ($num1<$num2) {
          for ($num=$num1; $num<=$num2; $num++) {
            echo "$num&#9;";
          }
        } else {
          for ($num=$num1; $num>=$num2; $num--) {
            echo "$num&#9;";
          }
        }
        ?></pre></code>
      </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
